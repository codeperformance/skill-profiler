DropDownOptions = {}
SelectedProfile = ""
CurrentSpecSlotID = 0
ShortTimeGate = 0.175
function report(table)
    for k, v in pairs(table) do
        print(k, v)
    end
end

function inArray(array, value)
    for i = 1, #array do
        if array[i] == value then
            return true
        end
    end
    return false
end

function getNumSpecializationUnlocked()
    local count = 0
    for specid, v in pairs(SPEC_SWAP_SPELLS) do
        if CA_IsSpellKnown(v) then
            count = count + 1
        end
    end
    return count
end

function checkSpell(ID)
    local minIndex = 0
    local amount = 20
    for i = minIndex,minIndex+amount do

        C_Timer.After(i - minIndex, function()
            print(i..": "..(C_Spell.GetSpellID(2912, i, 1) or ""))
            --CA_LearnSpell(i)
        end)
    end
end

function getSpellIDByName(name)
    local splitSpellLink = string.gmatch(GetSpellLink(name), '([^|]+)')--split by pipe |
    local count = 0
    for spellLinkComponent in splitSpellLink do
        if count == 1 then
            for number in string.gmatch(spellLinkComponent, '(%d+)') do--select complete number
                return number
            end
        end
        count = count + 1
    end
end

function scanKnownTalents()
    local knownTalents = {}
    local talentEntries = C_CharacterAdvancement.GetKnownTalentEntries()
    for _, talent in pairs(talentEntries) do
        knownTalents[talent.ID] =C_CharacterAdvancement.GetTalentRankByID(talent.ID)
    end
    return knownTalents
end

function scanKnownSpells()
    local knownSpells = {}
    local spellEntries = C_CharacterAdvancement.GetKnownSpellEntries()
    for _, spell in pairs(spellEntries) do
        table.insert(knownSpells, spell.ID)
    end
    return knownSpells
end

function scanKnownMysticEnchants()
    local knownMysticEnchants = {}
    for i=1,17 do
        knownMysticEnchants[i..""] = {}
        knownMysticEnchants[i..""] = C_MysticEnchant.GetAppliedEnchant("PLAYER",i)
    end
    return knownMysticEnchants
end

--learning spells and talents
function learnInternalID(InternalID)
    C_CharacterAdvancement.LearnID(InternalID)
end

function unlearnInternalID(InternalID)
    C_CharacterAdvancement.UnlearnID(InternalID)
end
--learning and commiting mystic enchants
function applyMysticEnchantToSlot(slot, mysticEnchantSpellID)
    C_MysticEnchant.CollectionReforgeSlot(slot, mysticEnchantSpellID)
end

function commitMysticEnchantChanges()
    C_MysticEnchant.SaveCollectionReforge()
end

function removeMysticEnchantFromSlot(SlotID)
    C_MysticEnchant.Destroy(SlotID)
end

function getSpellIDByName(Name)
    return C_Spell:GetSpellID(Name, "rank 1")
end

function getInternalIDBySpellID(SpellID)
    return CharacterAdvancementUtil.GetIDBySpellID(SpellID)
end

function getSpellIDByInternalID(InternalID)
    return C_CharacterAdvancement.GetEntryByInternalID(InternalID).Spells[1]
end

function loadProfile(specID, profileName)
    return SkillProfiler["specs"][specID]["profiles"][profileName]
end

function storeProfile(specID, profileName, profile)

    if SkillProfiler == nil then
        SkillProfiler = {}
    end
    if SkillProfiler["specs"] == nil then
        SkillProfiler["specs"] = {}
    end
    if SkillProfiler["specs"][specID] == nil then
        SkillProfiler["specs"][specID] = {}
    end
    if SkillProfiler["specs"][specID]["profiles"] == nil then
        SkillProfiler["specs"][specID]["profiles"] = {}
    end
    SkillProfiler["specs"][specID]["profiles"][profileName] = profile
end

function clearProfile(specID, profileName)
    if SkillProfiler == nil then
        SkillProfiler = {}
    end
    if SkillProfiler["specs"] == nil then
        SkillProfiler["specs"] = {}
    end
    if SkillProfiler["specs"][specID] == nil then
        SkillProfiler["specs"][specID] = {}
    end
    if SkillProfiler["specs"][specID]["profiles"] == nil then
        SkillProfiler["specs"][specID]["profiles"] = {}
    end
    SkillProfiler["specs"][specID]["profiles"][profileName] = nil
end

function storeData(key, value)
    if SkillProfiler == nil then
        SkillProfiler = {}
    end
    SkillProfiler[key] = value
end

function loadData(key)
    if SkillProfiler == nil then
        SkillProfiler = {}
    end
    return SkillProfiler[key]
end

function generateProfile()
    local profile = {}
    profile["spells"] = scanKnownSpells()
    profile["talents"] = scanKnownTalents()
    profile["enchants"] = scanKnownMysticEnchants()
    return profile
end

function shareBuild()
    --check Interface/AddOns/WIM/Sources/Socket.lua
end

function loadProfiles(specID)
    local specs = loadData("specs")
    if specs ~= nil and specs[specID] ~= nil and specs[specID]["profiles"] ~= nil  then
        return specs[specID]["profiles"]
    end
    return {}
end

function initializeDropDownOptions()
    DropDownOptions = {}
    local profiles = loadProfiles(CurrentSpecSlotID)
    for k, v in pairs(profiles) do
        table.insert(DropDownOptions, k)
    end
end

buttonW = 50
buttonH = 25


--find which spell2 is not in spell1
function spellDiff(spell1, spell2)
    local diff = {}
    for i = 1, #spell1 do
        local found = false
        for ii = 1, #spell2 do
            if spell1[i] == spell2[ii] then
                found = true
            end
        end
        if not found then
            table.insert(diff, spell1[i])
        end
    end
    return diff
end

--find which enchant2 is not in enchant1
function enchantDiff(enchant1, enchant2)
    local diff = {}
    for ID, SLOTID in pairs(enchant1) do
        local stackDiff = 0
        local found = false
        for ID2, SLOTID2 in pairs(enchant2) do
            if ID == ID2 then
                if SLOTID == SLOTID2 then
                    found = true
                end
            end
        end


        if not found then
            diff[ID] = {}
            diff[ID] = SLOTID
        end
    end
    return diff
end

--find which talent2 is not in talent1
function talentDiff(talent1, talent2)
    local diff = {}
    for ID, STACKS in pairs(talent1) do
        local found = false
        for ID2, STACK2 in pairs(talent2) do
            if ID == ID2 then
                if STACKS == STACK2 then
                    found = true
                end
            end
        end
        if not found then
            diff[ID] = {}
            diff[ID] = STACKS
        end
    end
    return diff
end

function unlearnSpells(timer, spellDiff)
    for i = 1, #spellDiff do
        C_Timer.After(timer, function()
            unlearnInternalID(spellDiff[i])
        end)
        timer = timer + ShortTimeGate
    end
    return timer
end

function learnSpells(timer, spellDiff)
    for i = 1, #spellDiff do
        C_Timer.After(timer, function()
            learnInternalID(spellDiff[i])
        end)
        timer = timer + ShortTimeGate
    end
    return timer
end

function unlearnTalents(timer, talentDiff)
    for talentSpellID, _ in pairs(talentDiff) do

        C_Timer.After(timer, function()
            unlearnInternalID(talentSpellID)
        end)
        timer = timer + ShortTimeGate
    end
    return timer
end

function learnTalents(timer, talentDiff)
    for talentSpellID, rank in pairs(talentDiff) do
        for i = 1, rank do
            C_Timer.After(timer, function()
                learnInternalID(talentSpellID)
            end)
            timer = timer + ShortTimeGate
        end
    end
    return timer
end

function learnEnchants(timer, enchantDiff)
    for slotID, enchantSpellID in pairs(enchantDiff) do
        if enchantSpellID == 0 then
            C_Timer.After(timer, function()
                removeMysticEnchantFromSlot(SlotID)
            end)
        else
            C_Timer.After(timer, function()
                applyMysticEnchantToSlot(slotID, enchantSpellID)
            end)
        end
        timer = timer + ShortTimeGate
    end
    C_Timer.After(timer, function()
        commitMysticEnchantChanges()
    end)
    return timer + 1
end

function unlearnEnchants(timer, enchantDiff)
    for slotID, enchantSpellID in pairs(enchantDiff) do
        if enchantSpellID == 0 then
            C_Timer.After(timer, function()
                removeMysticEnchantFromSlot(SlotID)
            end)
            timer = timer + 1
        end
    end
    return timer
end

function getProfileDiff(currentProfile, selectedProfile)
    local profileDiff = {}
    profileDiff["unlearnSpellDiff"] = spellDiff(currentProfile.spells, selectedProfile.spells)
    profileDiff["unlearnTalentDiff"] = talentDiff(currentProfile.talents, selectedProfile.talents)
    profileDiff["unlearnEnchantDiff"] = enchantDiff(currentProfile.enchants, selectedProfile.enchants)
    profileDiff["learnSpellDiff"] = spellDiff(selectedProfile.spells, currentProfile.spells)
    profileDiff["learnTalentDiff"] = talentDiff(selectedProfile.talents, currentProfile.talents)
    profileDiff["learnEnchantDiff"] = enchantDiff(selectedProfile.enchants, currentProfile.enchants)
    return profileDiff
end

function switchToProfile(specslotID, selectedProfileName)
    local selectedProfile = loadProfile(specslotID, selectedProfileName)
    local currentProfile = generateProfile()

    local profileDiff = getProfileDiff(currentProfile, selectedProfile)

    local timer = 0.0
    print("Unlearn spells")
    report(profileDiff.unlearnSpellDiff)
    timer = unlearnSpells(timer, profileDiff.unlearnSpellDiff)
    print(timer)
    print("=====")
    print("Unlearn talents")
    report(profileDiff.unlearnTalentDiff)
    timer = unlearnTalents(timer, profileDiff.unlearnTalentDiff)
    print(timer)
    --print("=====")
    --print("Unlearn enchants")
    --report(unlearnEnchantDiff)
    --timer = unlearnEnchants(timer, unlearnEnchantDiff)
    --print(timer)
    print("=====")
    print("Learn spells")
    timer = learnSpells(timer, profileDiff.learnSpellDiff)
    report(profileDiff.learnSpellDiff)
    print(timer)
    print("=====")
    print("Learn talents")
    report(profileDiff.learnTalentDiff)
    timer = learnTalents(timer, profileDiff.learnTalentDiff)
    print(timer)
    print("=====")
    --print("Learn enchants")
    --report(learnEnchantDiff)
    --timer = learnEnchants(timer, learnEnchantDiff)
    --print(timer)
    --print("=====")
    print(timer)
    C_Timer.After(timer, function()
        print("Profile loaded")
        if ActionBarSaver then
            ActionBarSaver:RestoreProfile(specslotID.."."..selectedProfileName)
        end
    end)
end

function createButton(name, width, height, lockTo, selfAncorPoint, parentAnchorpoint, x, y)
    local button = CreateFrame("Button", "BTN_"..name, lockTo, "GameMenuButtonTemplate")
    button:SetPoint(selfAncorPoint, lockTo, parentAnchorpoint, x, y)
    button:SetSize(width, height)
    button:SetText(name)
    button:SetNormalFontObject("GameFontNormalLarge")
    button:SetHighlightFontObject("GameFontHighlightLarge")
    return button
end
specslotDropdown = nil
profileDropdown = nil
diffLabel = nil

function OnClick(v)
    SelectedProfile = v
    UIDropDownMenu_SetSelectedValue(profileDropdown, v)
    local selectedProfile = loadProfile(CurrentSpecSlotID, SelectedProfile)
    local currentProfile = generateProfile()
    local profileDiff = getProfileDiff(currentProfile, selectedProfile)
    local cost = 0
    local labelText = "Unlearning spells:\n"
    for _, internalSpellID in pairs(profileDiff.unlearnSpellDiff) do
        local spellName = GetSpellInfo(getSpellIDByInternalID(internalSpellID))
        labelText = labelText..spellName.."\n"
        cost = cost + 1
    end
    labelText = labelText.."\nUnlearning talents:\n"
    for talentSpellID, _ in pairs(profileDiff.unlearnTalentDiff) do
        local spellName = GetSpellInfo(getSpellIDByInternalID(talentSpellID))
        labelText = labelText..spellName.."\n"
        cost = cost + 1
    end
    labelText = labelText.."\nLearning spells:\n"
    for _, internalSpellID in pairs(profileDiff.learnSpellDiff) do
        local spellName = GetSpellInfo(getSpellIDByInternalID(internalSpellID))
        labelText = labelText..spellName.."\n"
    end
    labelText = labelText.."\nLearning talents:\n"
    for talentSpellID, _ in pairs(profileDiff.learnTalentDiff) do
        local spellName = GetSpellInfo(getSpellIDByInternalID(talentSpellID))
        labelText = labelText..spellName.."\n"
    end
    labelText = labelText.."\nCost: "..cost.." "..GetItemLink(1101243).."\n"
    --diffLabel:SetText(labelText)
    diffLabel:SetText("Cost: "..cost.." "..GetItemLink(1101243))
end

local function InitializeDropDown()
    local info = UIDropDownMenu_CreateInfo()
    if #DropDownOptions == 0 then
        DropDownOptions["Select"] = "Select"
    end

    for k,v in pairs(DropDownOptions) do
        info = UIDropDownMenu_CreateInfo()
        info.text = v
        info.value = v
        info.func = function()
            OnClick(v)
        end
        UIDropDownMenu_AddButton(info)
    end
end
function initUI()

    local visibility = loadData("visibility") or false
    local frame = CreateFrame("Frame", "MyAddonMainFrame", CharacterAdvancement)
    frame:SetSize(300, CharacterAdvancement:GetHeight())
    frame:SetPoint("TOPLEFT", CharacterAdvancement, "TOPRIGHT")
    frame:SetBackdrop({
        bgFile = "Interface\\DialogFrame\\UI-DialogBox-Background",
        edgeFile = "Interface\\DialogFrame\\UI-DialogBox-Border",
        tile = true, tileSize = 32, edgeSize = 32,
        insets = { left = 11, right = 12, top = 12, bottom = 11 }
    })
    frame:EnableMouse(true)
    frame:SetBackdropColor(0, 0, 0, 1)
    if visibility then
        frame:Show()
        else
        frame:Hide()
    end

    local togglePanelButton = createButton(
            "Profiler",
            80,
            22,
            CharacterAdvancement,
            "TOPRIGHT",
            "TOPRIGHT",
            -22,
            0
    )

    -- Create the dropdown
    local newButton = createButton(
            "New",
            270,
            27,
            frame,
            "TOPLEFT",
            "TOPLEFT",
            15,
            -15
    )

    local saveButton = createButton(
            "Save",
            270,
            27,
            newButton,
            "TOPLEFT",
            "BOTTOMLEFT",
            0,
            0
    )

    local loadButton = createButton(
            "Load",
            270,
            27,
            saveButton,
            "TOPLEFT",
            "BOTTOMLEFT",
            0,
            0
    )
    --local copyButton = createButton(
    --        "Copy",
    --        270,
    --        27,
    --        loadButton,
    --        "TOPLEFT",
    --        "BOTTOMLEFT",
    --        0,
    --        0
    --)
    local removeButton = createButton(
            "Remove",
            270,
            27,
            loadButton,
            "TOPLEFT",
            "BOTTOMLEFT",
            0,
            0
    )

    local renameButton = createButton(
            "Rename",
            270,
            27,
            removeButton,
            "TOPLEFT",
            "BOTTOMLEFT",
            0,
            0
    )

    local searchBox = CreateFrame("Editbox", nil, renameButton, "InputBoxTemplate")
    searchBox:SetPoint("TOPLEFT", renameButton, "BOTTOMLEFT", 10, 0)
    searchBox:SetSize(130, 27)
    searchBox:SetAutoFocus(false)

    local searchButton = createButton(
            "Search",
            130,
            27,
            searchBox,
            "TOPLEFT",
            "TOPRIGHT",
            0,
            0
    )
    specslotDropdown = CreateFrame("Frame", "SpecslotDropdown", frame, "UIDropDownMenuTemplate")
    specslotDropdown:SetPoint("TOPLEFT", searchBox, "BOTTOMLEFT", -23, 0)
    UIDropDownMenu_SetWidth(specslotDropdown, 250)

    profileDropdown = CreateFrame("Frame", "MyAddonDropdown", frame, "UIDropDownMenuTemplate")
    profileDropdown:SetPoint("TOPLEFT", specslotDropdown, "BOTTOMLEFT")
    UIDropDownMenu_SetWidth(profileDropdown, 250)

    diffLabel = frame:CreateFontString(nil, "OVERLAY")
    diffLabel:SetFont("Fonts\\FRIZQT__.TTF", 11, "OUTLINE")
    diffLabel:SetText("")
    diffLabel:SetJustifyH("LEFT")
    diffLabel:SetPoint("TOPLEFT", profileDropdown, "BOTTOMLEFT", 20, 0)

    UIDropDownMenu_Initialize(specslotDropdown, function()
        for i = 1, getNumSpecializationUnlocked() do
            info = UIDropDownMenu_CreateInfo()
            info.text = "Spec "..i
            info.value = i
            info.func = function()
                UIDropDownMenu_SetSelectedValue(specslotDropdown, i)
                CurrentSpecSlotID = i
                initializeDropDownOptions()
                UIDropDownMenu_ClearAll(profileDropdown)
                UIDropDownMenu_Initialize(profileDropdown, InitializeDropDown)
                diffLabel:SetText("")
            end
            UIDropDownMenu_AddButton(info)
        end
    end)
    CurrentSpecSlotID = SpecializationUtil.GetActiveSpecialization()
    UIDropDownMenu_SetSelectedValue(specslotDropdown, CurrentSpecSlotID)




    initializeDropDownOptions()
    UIDropDownMenu_Initialize(profileDropdown, InitializeDropDown)
    togglePanelButton:SetScript("OnClick", function()
        if frame:IsVisible() then
            frame:Hide()
            visibility = false
        else
            frame:Show()
            visibility = true
        end
        storeData("visibility", visibility)


    end)
    newButton:SetScript("OnClick", function()
        StaticPopupDialogs["MY_POPUP"] = {
            text = "Enter the profile name",
            button1 = "Ok",
            button2 = "Cancel",
            OnShow = function (self2, data)
                self2.editBox:SetText("")
            end,
            OnAccept = function (self2, data, data2)
                local input = self2.editBox:GetText()
                DropDownOptions["Select"] = nil
                table.insert(DropDownOptions, input)
                -- Do some stuff with the input
                UIDropDownMenu_Initialize(profileDropdown, InitializeDropDown)
                UIDropDownMenu_SetSelectedID(profileDropdown, #DropDownOptions)
                SelectedProfile = input
            end,
            hasEditBox = true,
            timeout = 0,
            whileDead = true,
            hideOnEscape = true,
            preferredIndex = 3,
        }
        StaticPopup_Show ("MY_POPUP")
    end)
    loadButton:SetScript("OnClick", function()
        print("Loading "..SelectedProfile)
        StaticPopupDialogs["R_U_SURE_LOAD"] = {
            text = "Are you sure you want to load/override current spec with "..SelectedProfile,
            button1 = "Ok",
            button2 = "Cancel",
            OnAccept = function ()
                switchToProfile(CurrentSpecSlotID, SelectedProfile)

            end,
            timeout = 0,
            whileDead = true,
            hideOnEscape = true,
            preferredIndex = 3,
        }

        StaticPopup_Show ("R_U_SURE_LOAD")
    end)
    saveButton:SetScript("OnClick", function()
        StaticPopupDialogs["R_U_SURE_SAVE"] = {
            text = "Are you sure you want to save/override "..SelectedProfile,
            button1 = "Ok",
            button2 = "Cancel",
            OnAccept = function (_, data, data2)
                local profile = generateProfile()
                storeProfile(CurrentSpecSlotID, SelectedProfile, profile)
                if ActionBarSaver then
                    ActionBarSaver:SaveProfile(CurrentSpecSlotID.."."..SelectedProfile)
                end

            end,
            timeout = 0,
            whileDead = true,
            hideOnEscape = true,
            preferredIndex = 3,
        }

        StaticPopup_Show ("R_U_SURE_SAVE")
    end)

    --copyButton:SetScript("OnClick", function()
    --    StaticPopupDialogs["COPY_POPUP"] = {
    --        text = "Enter the copy name",
    --        button1 = "Ok",
    --        button2 = "Cancel",
    --        OnShow = function (self2, data)
    --            self2.editBox:SetText("")
    --        end,
    --        OnAccept = function (self2, data, data2)
    --            local input = self2.editBox:GetText()
    --            local profile = loadProfile(CurrentSpecSlotID, SelectedProfile)
    --            storeProfile(CurrentSpecSlotID, input, profile)
    --            initializeDropDownOptions()
    --            UIDropDownMenu_Initialize(profileDropdown, InitializeDropDown)
    --            SelectedProfile = input
    --            UIDropDownMenu_SetSelectedValue(profileDropdown, SelectedProfile)
    --        end,
    --        hasEditBox = true,
    --        timeout = 0,
    --        whileDead = true,
    --        hideOnEscape = true,
    --        preferredIndex = 3,
    --    }
    --    StaticPopup_Show ("COPY_POPUP")
    --end)

    removeButton:SetScript("OnClick", function()
        StaticPopupDialogs["R_U_SURE_DELETE"] = {
            text = "Are you sure you want to delete "..SelectedProfile,
            button1 = "Ok",
            button2 = "Cancel",
            OnAccept = function (_, data, data2)
                clearProfile(CurrentSpecSlotID, SelectedProfile)
                initializeDropDownOptions()
                UIDropDownMenu_ClearAll(profileDropdown)
                UIDropDownMenu_Initialize(profileDropdown, InitializeDropDown)
            end,
            timeout = 0,
            whileDead = true,
            hideOnEscape = true,
            preferredIndex = 3,
        }

        StaticPopup_Show ("R_U_SURE_DELETE")
    end)
    renameButton:SetScript("OnClick", function()
        StaticPopupDialogs["RENAME_POPUP"] = {
            text = "Enter the new name",
            button1 = "Ok",
            button2 = "Cancel",
            OnShow = function (self2, data)
                self2.editBox:SetText("")
            end,
            OnAccept = function (self2, data, data2)
                local input = self2.editBox:GetText()
                local profile = loadProfile(CurrentSpecSlotID, SelectedProfile)
                storeProfile(CurrentSpecSlotID, input, profile)
                clearProfile(CurrentSpecSlotID, SelectedProfile)
                initializeDropDownOptions()
                UIDropDownMenu_Initialize(profileDropdown, InitializeDropDown)
                SelectedProfile = input
                UIDropDownMenu_SetSelectedValue(profileDropdown, SelectedProfile)
            end,
            hasEditBox = true,
            timeout = 0,
            whileDead = true,
            hideOnEscape = true,
            preferredIndex = 3,
        }
        StaticPopup_Show ("RENAME_POPUP")
    end)
    function RunSearch()
        searchBox:ClearFocus()
        searchBox:GetText()
        UIDropDownMenu_ClearAll(profileDropdown)
        UIDropDownMenu_Initialize(profileDropdown, function()
            local info = UIDropDownMenu_CreateInfo()
            for k,v in pairs(DropDownOptions) do
                if v ~= nil and string.find(v:lower(), searchBox:GetText():lower()) then
                    info = UIDropDownMenu_CreateInfo()
                    info.text = v
                    info.value = v
                    info.func = function()
                        UIDropDownMenu_SetSelectedValue(profileDropdown, v)
                        SelectedProfile = v
                        OnClick(v)
                    end
                    UIDropDownMenu_AddButton(info)
                end
            end
        end)
        diffLabel:SetText("")
    end
    searchButton:SetScript("OnClick", function()
        RunSearch()
    end)
    searchBox:SetScript("OnEnterPressed", function()
        RunSearch()
    end)
    --frame:SetScale(0.75)
end


eventFrame = CreateFrame("FRAME", "SkillProfilerFrame");
eventFrame:RegisterEvent("PLAYER_LOGIN");
eventFrame:RegisterEvent("ASCENSION_CA_SPECIALIZATION_ACTIVE_ID_CHANGED");
function eventHandler(self, event)
    if event == "PLAYER_LOGIN" then
        CurrentSpecSlotID = SpecializationUtil.GetActiveSpecialization()
        --UIDropDownMenu_SetSelectedValue(specslotDropdown, CurrentSpecSlotID)
    end
    if event == "ASCENSION_CA_SPECIALIZATION_ACTIVE_ID_CHANGED" then
        CurrentSpecSlotID = SpecializationUtil.GetActiveSpecialization()
        if loaded then
            UIDropDownMenu_SetSelectedValue(specslotDropdown, CurrentSpecSlotID)
            UIDropDownMenu_SetText(specslotDropdown, "Spec "..CurrentSpecSlotID)
            initializeDropDownOptions()
            UIDropDownMenu_ClearAll(profileDropdown)
            UIDropDownMenu_Initialize(profileDropdown, InitializeDropDown)
            diffLabel:SetText("")
        end
    end
end
loaded = false
eventFrame:SetScript("OnUpdate", function(self, elapsed)
    local frame_to_check = _G["CharacterAdvancement"]
    --only init when CharacterAdvancement panel exists for anchoring
    if frame_to_check and not loaded then
        loaded = true

        initUI()
    end
end)
eventFrame:SetScript("OnEvent", eventHandler);